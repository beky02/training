import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:training/components/map_pin_pill.dart';
import 'dart:async';

import 'models/pin_pill_info.dart';

const double CAMERA_ZOOM = 13;
const double CAMERA_TILT = 0;
const double CAMERA_BEARING = 30;
const LatLng SOURCE_LOCATION = LatLng(42.7477863, -71.1699932);
const LatLng DEST_LOCATION = LatLng(42.6871386, -71.2143403);

class MapPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => MapPageState();
}

class MapPageState extends State<MapPage> {
  Completer<GoogleMapController> _controller = Completer();
  Set<Marker> _markers = {};
  Set<Polyline> _polylines = {};
  List<LatLng> polylineCoordinates = [];
  // PolylinePoints polylinePoints = PolylinePoints();
  // String googleAPIKey = "AIzaSyAruGVrQ10uY258z1rWu-8mx8MH0g5RscI";
  BitmapDescriptor sourceIcon;
  // BitmapDescriptor destinationIcon;
  // double pinPillPosition = -100;
  // PinInformation currentlySelectedPin = PinInformation(pinPath: '', avatarPath: '', location: LatLng(0, 0), locationName: '', labelColor: Colors.grey);
  // PinInformation sourcePinInfo;
  // PinInformation destinationPinInfo;
  static  LatLng _center = LatLng(10.3296, 37.7344);
  LatLng _lastMapPosition = _center;
  MapType _currentMapType = MapType.normal;
  

  void _onMapTypeButtonPressed() {
    setState(() {
      _currentMapType = _currentMapType == MapType.normal
          ? MapType.satellite
          : MapType.normal;
    });
  }

  void _onAddMarkerButtonPressed() {
    setState(() {
      _markers.add(Marker(
          markerId: MarkerId(_lastMapPosition.toString()),
          draggable: true,
          position: _lastMapPosition,
          infoWindow: InfoWindow(
            title: 'Custom Marker',
            snippet: 'Inducesmile.com',
          ),
          icon: BitmapDescriptor.defaultMarker));
    });
  }

  void _onCameraMove(CameraPosition position) {
    _lastMapPosition = position.target;
    CameraPosition newPos = CameraPosition(target: position.target);
    Marker marker = _markers.first;
    setState(() {
      _markers.first.copyWith(positionParam: newPos.target);
    });
  }

  void _onMapCreated(GoogleMapController controller) {
    _controller.complete(controller);
  }

  Future<void> getLocation() async{
    PermissionStatus permission = await PermissionHandler().checkPermissionStatus(PermissionGroup.location);
    if (permission == PermissionStatus.denied) {
      await PermissionHandler().requestPermissions([PermissionGroup.locationAlways]);
    }

    var geolocator = Geolocator();
    GeolocationStatus geolocationStatus = await geolocator.checkGeolocationPermissionStatus();
    switch (geolocationStatus) {
      case GeolocationStatus.denied:
        print("denied");
        break;
      case GeolocationStatus.disabled:
      case GeolocationStatus.restricted:
        print('restricted');
        break;
      case GeolocationStatus.unknown:
        print('unknown');
        break;
      case GeolocationStatus.granted:
        await Geolocator().getCurrentPosition(desiredAccuracy: LocationAccuracy.high).then((Position _position) {
          if (_position != null) {
            setState(() {
              _center = LatLng(_position.latitude, _position.longitude);
            });
            print(_position);
          }
        });
    }
  }

  @override
  void initState() {
    super.initState();
    // setSourceAndDestinationIcons();
    _onAddMarkerButtonPressed();
    // getLocation();
  }

  // void setMapPins() {
  //      // source pin
  //     _markers.add(Marker(

  //     // This marker id can be anything that uniquely identifies each marker.
  //       markerId: MarkerId('sourcePin'),
  //       position: SOURCE_LOCATION,
  //       onTap: () {
  //         setState(() {
  //           currentlySelectedPin = sourcePinInfo;
  //           pinPillPosition = 0;
  //         });
  //       },
  //       icon: sourceIcon
  //     ));

  //     sourcePinInfo = PinInformation(
  //       locationName: "Start Location",
  //       location: SOURCE_LOCATION,
  //       pinPath: "assets/driving_pin.png",
  //       avatarPath: "assets/friend1.jpg",
  //       labelColor: Colors.blueAccent
  //     );

  //     // destination pin
  //     _markers.add(Marker(
  //       // This marker id can be anything that uniquely identifies each marker.
  //       markerId: MarkerId('destPin'),
  //       position: DEST_LOCATION,
  //       onTap: () {
  //         setState(() {
  //           currentlySelectedPin = destinationPinInfo;
  //           pinPillPosition = 0;
  //         });
  //       },
  //       icon: destinationIcon
  //     ));

  //     destinationPinInfo = PinInformation(
  //       locationName: "End Location",
  //       location: DEST_LOCATION,
  //       pinPath: "assets/destination_map_marker.png",
  //       avatarPath: "assets/friend2.jpg",
  //       labelColor: Colors.purple
  //     );
  //   }

  // void setSourceAndDestinationIcons() async {
  //   sourceIcon = await BitmapDescriptor.fromAssetImage(
  //       ImageConfiguration(devicePixelRatio: 2.5), 'assets/driving_pin.png');

  //   destinationIcon = await BitmapDescriptor.fromAssetImage(
  //       ImageConfiguration(devicePixelRatio: 2.5), 'assets/destination_map_marker.png');
  // }

  // void onMapCreated(GoogleMapController controller) {
  //   controller.setMapStyle(Utils.mapStyles);
  //   _controller.complete(controller);

  //   setMapPins();
  //   setPolylines();
  // }

  @override
  Widget build(BuildContext context) {
    // CameraPosition initialLocation = CameraPosition(
    //     zoom: CAMERA_ZOOM,
    //     bearing: CAMERA_BEARING,
    //     tilt: CAMERA_TILT,
    //     target: SOURCE_LOCATION);

    return Scaffold(
        body: Stack(children: <Widget>[
      GoogleMap(
          onMapCreated: _onMapCreated,
          mapType: _currentMapType,
          markers: _markers,
          myLocationEnabled: true,
          onCameraMove: _onCameraMove,
          initialCameraPosition: CameraPosition(target: _center, zoom: 8.0)),
      Padding(
        padding: EdgeInsets.all(16),
        child: Align(
          alignment: Alignment.topRight,
          child: Column(children: <Widget>[
            FloatingActionButton(
              onPressed: _onMapTypeButtonPressed,
              materialTapTargetSize: MaterialTapTargetSize.padded,
              heroTag: 'unq1',
              backgroundColor: Colors.green,
              child: Icon(Icons.map, size: 36.0),),
              SizedBox(height: 16.0),
              FloatingActionButton(
              onPressed: _onAddMarkerButtonPressed,
              materialTapTargetSize: MaterialTapTargetSize.padded,
              heroTag: 'unq2',
              backgroundColor: Colors.green,
              child: Icon(Icons.add_location, size: 36.0),),
          ],)
        ),
      )
    ]));
  }

//   setPolylines() async
//   {
//     List<PointLatLng> result = await polylinePoints.getRouteBetweenCoordinates(googleAPIKey,
//         SOURCE_LOCATION.latitude, SOURCE_LOCATION.longitude,
//         DEST_LOCATION.latitude, DEST_LOCATION.longitude);
//         print(polylineCoordinates);

//     if(result.isNotEmpty){
//       result.forEach((PointLatLng point){
//         polylineCoordinates.add(LatLng(point.latitude, point.longitude));
//       });

//       setState(() {
//         Polyline polyline = Polyline(
//             polylineId: PolylineId("poly"),
//             color: Color.fromARGB(255, 40, 122, 198),
//             points: polylineCoordinates
//         );
//         _polylines.add(polyline);
//       });
//     }
//   }
}

class Utils {
  static String mapStyles = '''[
  {
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#f5f5f5"
      }
    ]
  },
  {
    "elementType": "labels.icon",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "color": "#f5f5f5"
      }
    ]
  },
  {
    "featureType": "administrative.land_parcel",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#bdbdbd"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#eeeeee"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#e5e5e5"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#ffffff"
      }
    ]
  },
  {
    "featureType": "road.arterial",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#dadada"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "featureType": "road.local",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  },
  {
    "featureType": "transit.line",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#e5e5e5"
      }
    ]
  },
  {
    "featureType": "transit.station",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#eeeeee"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#c9c9c9"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  }
]''';
}
