import 'package:flutter/material.dart';

class ParkingHistory extends StatelessWidget {
  final List<String> entries = <String>[
    'A',
    'B',
    'C',
    'D',
    'E',
    'F',
    'G',
    'H',
    'I',
    'J',
    'K',
    'L',
    'M'
  ];
  final List<int> colorCodes = <int>[600, 500, 50];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        title: Text("Parking History"),
      ),
      body: Stack(
        children: <Widget>[
          Container(
            color: Color(0xff4F5DF6),
            height: 300.0,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 15.0, right: 15.0, top: 180.0),
            child: Container(
              height: double.infinity,
              width: double.infinity,
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(50),
                      topRight: Radius.circular(50))),
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 20.0, horizontal: 50.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Icon(
                          Icons.arrow_downward,
                          size: 30.0,
                          color: Color(0xff4F5DF6),
                        ),
                        Icon(
                          Icons.date_range,
                          color: Color(0xff4F5DF6),
                          size: 30.0,
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    child: ListView.separated(
                      padding: const EdgeInsets.all(10),
                      itemCount: entries.length,
                      itemBuilder: (BuildContext context, int index) {
                        return Container(
                          height: 80,
                          color: Colors.black12,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              SizedBox(width: 20),
                              Row(
                                children: <Widget>[
                                  Text(
                                    '45Br',
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 22),
                                  ),
                                  SizedBox(width: 10),
                                  CircleAvatar(
                                    backgroundImage: NetworkImage(
                                        "https://image.shutterstock.com/z/stock-vector-person-gray-photo-placeholder-man-in-a-costume-on-white-background-1259815156.jpg"),
                                    radius: 30.0,
                                  ),
                                ],
                              ),
                              SizedBox(width: 40),
                              Expanded(
                                child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Row(
                                      children: <Widget>[
                                        Text(
                                          'Date: ',
                                          style: TextStyle(fontSize: 16),
                                        ),
                                        Text('4th 2019',
                                            style: TextStyle(
                                                fontSize: 16,
                                                fontWeight: FontWeight.w200)),
                                      ],
                                    ),
                                    Row(
                                      children: <Widget>[
                                        Text('Duration: ',
                                            style: TextStyle(fontSize: 16)),
                                        Text('3:40:23',
                                            style: TextStyle(
                                                fontSize: 16,
                                                fontWeight: FontWeight.w200)),
                                      ],
                                    ),
                                    Row(
                                      children: <Widget>[
                                        Text('User: ',
                                            style: TextStyle(fontSize: 16)),
                                        Text('Abel Tilahun',
                                            style: TextStyle(
                                                fontSize: 16,
                                                fontWeight: FontWeight.w200)),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        );
                      },
                      separatorBuilder: (BuildContext context, int index) =>
                          const Divider(),
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
