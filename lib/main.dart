import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:training/Auth/Authentication.dart';
import 'package:training/Call.dart';
import 'package:training/ConfirmLogin.dart';
import 'package:training/Login.dart';
import 'Home.dart';
import 'package:firebase_auth/firebase_auth.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(auth: Auth()),
      // initialRoute: '/',
      // routes: {
      //   '/': (context) => Home(),
      //   // '/profile': (context) => Profile()
      // },
    );
  }
}
enum AuthStatus {
  NOT_DETERMINED,
  NOT_LOGGED_IN,
  LOGGED_IN,
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title, this.auth}) : super(key: key);
  final String title;
  BaseAuth auth;
  

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  AuthStatus authStatus = AuthStatus.NOT_DETERMINED;
  String _userId;
  
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    widget.auth.getCurrentUser().then((user){
      setState(() {
        if (user != null) {
          _userId = user.uid;
        }
        authStatus = user?.uid == null ? AuthStatus.NOT_LOGGED_IN : AuthStatus.LOGGED_IN;
      });
    });
    
    
  }
  
  @override
  Widget build(BuildContext context) {
    print(authStatus);
    switch (authStatus) {
      case AuthStatus.NOT_DETERMINED:
        return ConfirmLogin();
        break;
      case AuthStatus.NOT_LOGGED_IN:
        return Login(
           auth: widget.auth,
          loginCallback: loginCallback,
        );
        break;
      case AuthStatus.LOGGED_IN:
        if (_userId.length > 0 && _userId != null) {
          return Home(
            userId: _userId,
            auth: widget.auth,
            logoutCallback: logoutCallback,
          );
        } else
          return SpinKitCircle(color: Colors.black);
        break;
      default:
        return SpinKitCircle(color: Colors.black);
    }
  }
  void loginCallback() {
    widget.auth.getCurrentUser().then((user) {
      setState(() {
        _userId = user.uid.toString();
      });
    });
    setState(() {
      authStatus = AuthStatus.LOGGED_IN;
    });
  }

  void logoutCallback() {
    setState(() {
      authStatus = AuthStatus.NOT_LOGGED_IN;
      _userId = "";
      Auth().signOut();
    });
  }
}
