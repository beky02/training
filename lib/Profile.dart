import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> with SingleTickerProviderStateMixin {
  TabController _controller;
  final List<String> _images = <String>[
    'assets/parked_car1.jpeg',
  ];
  File _imagePicked;
  Future getImageFromGallery() async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);
    setState(() {
      _imagePicked = image;
    });
  }

  Future getImageFromCamera() async {
    var image = await ImagePicker.pickImage(source: ImageSource.camera);
    setState(() {
      _imagePicked = image;
    });
  }

  @override
  void initState() {
    super.initState();
    _controller = new TabController(length: 3, vsync: this);
  }

  void _showModalSheet() {
    showModalBottomSheet(
        context: context,
        shape: OutlineInputBorder(borderRadius: BorderRadius.circular(20)),
        builder: (builder) {
          return Container(
              height: MediaQuery.of(context).size.height / 5,
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20.0),
                child: Wrap(
                  alignment: WrapAlignment.center,
                  runAlignment: WrapAlignment.center,
                  children: <Widget>[
                    Text('Select Image', style: TextStyle(fontSize: 18)),
                    ListTile(
                      leading: Icon(Icons.camera_enhance),
                      title: Text('Camera'),
                      onTap: getImageFromCamera,
                    ),
                    ListTile(
                      leading: Icon(Icons.image),
                      title: Text('Gallery'),
                      onTap: getImageFromGallery,
                    ),
                  ],
                ),
              ));
        },
        elevation: 2);
  }
  void _showModalSheetParking() {
    showModalBottomSheet(
        context: context,
        shape: OutlineInputBorder(borderRadius: BorderRadius.circular(20)),
        builder: (builder) {
          return Container(
              height: MediaQuery.of(context).size.height / 5,
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20.0),
                child: Wrap(
                  alignment: WrapAlignment.center,
                  runAlignment: WrapAlignment.center,
                  children: <Widget>[
                    Text('Select Image', style: TextStyle(fontSize: 18)),
                    ListTile(
                      leading: Icon(Icons.camera_enhance),
                      title: Text('Camera'),
                      onTap: getImageFromCamera,
                    ),
                    ListTile(
                      leading: Icon(Icons.image),
                      title: Text('Gallery'),
                      onTap: getImageFromGallery,
                    ),
                  ],
                ),
              ));
        },
        elevation: 2);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        title: Text("Profile"),
      ),
      body: Stack(
        children: <Widget>[
          Container(
            color: Color(0xff4F5DF6),
            height: 300.0,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 15.0, right: 15.0, top: 180.0),
            child: Container(
              height: double.infinity,
              width: double.infinity,
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(50),
                      topRight: Radius.circular(50))),
              child: Padding(
                padding: const EdgeInsets.only(top: 120.0),
                child: Column(
                  children: <Widget>[
                    Container(
                      child: new TabBar(
                        controller: _controller,
                        labelColor: Color(0xff4F5DF6),
                        unselectedLabelColor: Colors.black,
                        // indicatorSize: TabBarIndicatorSize.label,
                        indicatorColor: Color(0xff4F5DF6),
                        indicatorWeight: 3.0,
                        indicator: UnderlineTabIndicator(
                            borderSide: BorderSide(
                                width: 10.0, color: Color(0xff4F5DF6)),
                            insets: EdgeInsets.symmetric(horizontal: 35.0)),
                        tabs: [
                          Tab(
                            child: Text(
                              "Report",
                              style: TextStyle(
                                  fontSize: 14, fontWeight: FontWeight.bold),
                            ),
                          ),
                          Tab(
                            child: Text("Profile Info",
                                style: TextStyle(
                                    fontSize: 14, fontWeight: FontWeight.bold)),
                          ),
                          Tab(
                            child: Text("Gallery",
                                style: TextStyle(
                                    fontSize: 14, fontWeight: FontWeight.bold)),
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                      child: new TabBarView(
                        controller: _controller,
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(
                                left: 20, right: 20, top: 20, bottom: 200),
                            child: Container(
                                decoration: BoxDecoration(
                                    color: Color(0xff4F5DF6),
                                    borderRadius: BorderRadius.circular(20)),
                                child: Padding(
                                  padding: const EdgeInsets.only(left: 60.0),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text("Address",
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 16,
                                              fontWeight: FontWeight.bold)),
                                      Text("Bole Medihaniyalem",
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 16,
                                              decoration:
                                                  TextDecoration.underline)),
                                      SizedBox(height: 15.0),
                                      Text("Capacity",
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 16,
                                              fontWeight: FontWeight.bold)),
                                      Text("12 Cars",
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 16,
                                          )),
                                      SizedBox(height: 15.0),
                                      Text("Parking Price",
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 16,
                                              fontWeight: FontWeight.bold)),
                                      Text("10 Br/hr",
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 16,
                                          )),
                                      SizedBox(height: 15.0),
                                      Text("Working Hours",
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 16,
                                              fontWeight: FontWeight.bold)),
                                      Text("Address",
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 16,
                                          )),
                                    ],
                                  ),
                                )),
                          ),
                          Padding(
                            padding: EdgeInsets.only(
                                left: 20, right: 20, top: 20, bottom: 200),
                            child: Container(
                                decoration: BoxDecoration(
                                    color: Color(0xff4F5DF6),
                                    borderRadius: BorderRadius.circular(20)),
                                child: Padding(
                                  padding: const EdgeInsets.only(left: 60.0),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text("Address",
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 16,
                                              fontWeight: FontWeight.bold)),
                                      Text("Bole Medihaniyalem",
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 16,
                                              decoration:
                                                  TextDecoration.underline)),
                                      SizedBox(height: 15.0),
                                      Text("Capacity",
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 16,
                                              fontWeight: FontWeight.bold)),
                                      Text("12 Cars",
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 16,
                                          )),
                                      SizedBox(height: 15.0),
                                      Text("Parking Price",
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 16,
                                              fontWeight: FontWeight.bold)),
                                      Text("10 Br/hr",
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 16,
                                          )),
                                      SizedBox(height: 15.0),
                                      Text("Working Hours",
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 16,
                                              fontWeight: FontWeight.bold)),
                                      Text("Address",
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 16,
                                          )),
                                    ],
                                  ),
                                )),
                          ),
                          Padding(
                            padding: EdgeInsets.only(
                                left: 20, right: 20, top: 20, bottom: 60),
                            child: Container(
                                decoration: BoxDecoration(
                                    color: Color(0xff4F5DF6),
                                    borderRadius: BorderRadius.circular(20)),
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 20.0, vertical: 20),
                                  child: Column(
                                    children: <Widget>[
                                      Container(
                                        width: double.infinity,
                                        child: FlatButton(
                                          onPressed: _showModalSheetParking,
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: <Widget>[
                                              Text(
                                                'Add new pic',
                                                style: TextStyle(fontSize: 18),
                                              ),
                                              SizedBox(width: 20),
                                              Icon(
                                                Icons.camera_enhance,
                                                color: Colors.black,
                                              )
                                            ],
                                          ),
                                          textColor: Colors.black,
                                          color: Colors.white,
                                          shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(30)),
                                        ),
                                      ),
                                      // Expanded(
                                      //   child: ListView.separated(

                                      //       itemBuilder: (BuildContext context,
                                      //           int index) {
                                      //         return Image.asset(
                                      //             _images[index]);
                                      //       },
                                      //       separatorBuilder:
                                      //           (BuildContext context,
                                      //                   int index) =>
                                      //               SizedBox(height: 0),
                                      //       itemCount: _images.length),
                                      // )
                                      SizedBox(height: 10),
                                      Image.asset(_images[0]),
                                       SizedBox(height: 10),
                                     _imagePicked == null ? Text('') :Image.file(_imagePicked, width: 300, height: 180, fit: BoxFit.cover),
                                    ],
                                  ),
                                )),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),

              // child: Padding(
              //   padding: const EdgeInsets.only(top:130),
              //   child: DefaultTabController(
              //       length: 3,
              //       child: Scaffold(

              //         appBar: AppBar(
              //           backgroundColor: Colors.transparent,

              //           elevation: 0,
              //           flexibleSpace: TabBar(

              //               unselectedLabelColor: Colors.redAccent,
              //               indicatorSize: TabBarIndicatorSize.label,
              //               indicator: BoxDecoration(
              //                 borderRadius: BorderRadius.circular(3),
              //                 color: Colors.redAccent,
              //               ),
              //               tabs: [
              //                 Tab(
              //                   child: Container(
              //                     decoration: BoxDecoration(
              //                       border: Border(
              //                         bottom: BorderSide(
              //                             width: 0.0, color: Colors.redAccent),
              //                       ),
              //                       color: Colors.white,
              //                     ),
              //                     child: Align(
              //                       alignment: Alignment.center,
              //                       child: Text(
              //                         "APPS",
              //                         style: TextStyle(color: Colors.redAccent),
              //                       ),
              //                     ),
              //                   ),
              //                 ),
              //                 Tab(
              //                   child: Container(
              //                     decoration: BoxDecoration(
              //                         borderRadius: BorderRadius.circular(0),
              //                         border: Border.all(
              //                             color: Colors.redAccent, width: 1)),
              //                     child: Align(
              //                       alignment: Alignment.center,
              //                       child: Text("MOVIES"),
              //                     ),
              //                   ),
              //                 ),
              //                 Tab(
              //                   child: Container(
              //                     decoration: BoxDecoration(
              //                         borderRadius: BorderRadius.circular(50),
              //                         border: Border.all(
              //                             color: Colors.redAccent, width: 1)),
              //                     child: Align(
              //                       alignment: Alignment.center,
              //                       child: Text("GAMES"),
              //                     ),
              //                   ),
              //                 ),
              //               ]),
              //         ),
              //         body: TabBarView(children: [
              //           Container(child: Icon(Icons.apps)),
              //           Padding(
              //             padding: EdgeInsets.only(
              //                 left: 20, right: 20, top: 20, bottom: 200),
              //             child: Container(
              //                 decoration: BoxDecoration(
              //                     color: Color(0xff4F5DF6),
              //                     borderRadius: BorderRadius.circular(20)),
              //                 child: Padding(
              //                   padding: const EdgeInsets.only(left: 60.0),
              //                   child: Column(
              //                     mainAxisAlignment: MainAxisAlignment.center,
              //                     crossAxisAlignment: CrossAxisAlignment.start,
              //                     children: <Widget>[
              //                       Text("Address",
              //                           style: TextStyle(
              //                               color: Colors.white,
              //                               fontSize: 16,
              //                               fontWeight: FontWeight.bold)),
              //                       Text("Bole Medihaniyalem",
              //                           style: TextStyle(
              //                               color: Colors.white,
              //                               fontSize: 16,
              //                               decoration:
              //                                   TextDecoration.underline)),
              //                       SizedBox(height: 15.0),
              //                       Text("Capacity",
              //                           style: TextStyle(
              //                               color: Colors.white,
              //                               fontSize: 16,
              //                               fontWeight: FontWeight.bold)),
              //                       Text("12 Cars",
              //                           style: TextStyle(
              //                             color: Colors.white,
              //                             fontSize: 16,
              //                           )),
              //                       SizedBox(height: 15.0),
              //                       Text("Parking Price",
              //                           style: TextStyle(
              //                               color: Colors.white,
              //                               fontSize: 16,
              //                               fontWeight: FontWeight.bold)),
              //                       Text("10 Br/hr",
              //                           style: TextStyle(
              //                             color: Colors.white,
              //                             fontSize: 16,
              //                           )),
              //                       SizedBox(height: 15.0),
              //                       Text("Working Hours",
              //                           style: TextStyle(
              //                               color: Colors.white,
              //                               fontSize: 16,
              //                               fontWeight: FontWeight.bold)),
              //                       Text("Address",
              //                           style: TextStyle(
              //                             color: Colors.white,
              //                             fontSize: 16,
              //                           )),
              //                     ],
              //                   ),
              //                 )),
              //           ),
              //           Icon(Icons.games),
              //         ]),
              //       )),
              // )
            ),
          ),
          Positioned(
            top: 120,
            left: 140,
            child: Column(
              children: <Widget>[
                Stack(
                  children: <Widget>[
                    CircleAvatar(
                      backgroundImage: AssetImage('assets/friend1.jpg'),
                      radius: MediaQuery.of(context).size.width / 7,
                    ),
                    Positioned.fill(
                      child: Align(
                          alignment: Alignment.bottomRight,
                          child: InkWell(
                            onTap: _showModalSheet,
                            child: Icon(
                              Icons.camera_enhance,
                              size: 30,
                            ),
                          )),
                    )
                  ],
                ),
                SizedBox(height: 5),
                Text(
                  "Bereket Tadege",
                  style: TextStyle(fontSize: 18),
                ),
                Text("Id-k123k123")
              ],
            ),
          ),
        ],
      ),
    );
  }
}
