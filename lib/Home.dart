import 'package:flutter/material.dart';
import 'package:training/GoogleMap.dart';
import 'package:training/Login.dart';
import 'package:training/Parking_History.dart';
import 'package:training/Parking_Request.dart';
import 'package:training/Profile.dart';
import 'package:training/Rating.dart';

import 'Auth/Authentication.dart';

class Home extends StatelessWidget {
  final BaseAuth auth;
  final String userId;
  final VoidCallback logoutCallback;

  Home({this.userId,this.auth, this.logoutCallback});
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    final title = 'EthioPark';
    
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: title,
      home: Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.white,
          title: Text(title,style: TextStyle(color: Colors.black)),
          actions: <Widget>[
            PopupMenuButton(
                onSelected: (value) {
                  if (value == 'Profile') {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => Profile()));
                  } else if (value == 'Logout') {
                    logoutCallback();
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => Login()));
                  }
                },
                icon: Icon(Icons.more_vert, color: Colors.black,),
                padding: EdgeInsets.zero,
                offset: Offset(0, 55),
                itemBuilder: (context) => [
                      PopupMenuItem(
                          value: 'Profile',
                          height: 35,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Icon(
                                Icons.account_circle,
                                color: Color(0xff6e48ec),
                              ),
                              Text(
                                "Profile",
                                style: TextStyle(color: Colors.black),
                              ),
                            ],
                          )),
                      PopupMenuItem(
                          value: 'Logout',
                          height: 35,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                             Image.asset('assets/logout_icon.png',width: 20,),
                              Text(
                                "Logout",
                                style: TextStyle(color: Colors.black),
                              ),
                            ],
                          ))
                    ])
          ],
        ),
        body: Column(
          children: <Widget>[
            SizedBox(height: 10.0),
            Expanded(
              flex: 1,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  width: double.infinity,
                  decoration: BoxDecoration(
                    color: const Color(0xff6e48ec),
                    gradient: LinearGradient(colors: [Color.fromARGB(255, 68, 103, 251),Color.fromARGB(255, 132, 52, 228)]),
                    border: Border.all(
                      color: const Color(0xff6e48ec),
                    ),
                    borderRadius: BorderRadius.circular(12),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          flex: 1,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              CircleAvatar(
                                backgroundImage: NetworkImage(
                                    "https://image.shutterstock.com/z/stock-vector-person-gray-photo-placeholder-man-in-a-costume-on-white-background-1259815156.jpg"),
                                radius: 30.0,
                              ),
                              SizedBox(height: 10.0),
                              Text(
                                "Bereket Tadege",
                                style: TextStyle(
                                    fontSize: 15.0, color: Colors.white),
                              ),
                            ],
                          ),
                        ),
                        VerticalDivider(
                          color: Colors.white,
                          thickness: 4.0,
                        ),
                        Expanded(
                          flex: 1,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 5.0),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    Icon(
                                      Icons.local_parking,
                                      color: Colors.white,
                                      size: 23.0,
                                    ),
                                    SizedBox(
                                      width: 12.0,
                                    ),
                                    Column(
                                      children: <Widget>[
                                        Text(
                                          "Booking made",
                                          style: TextStyle(
                                            fontSize: 16.0,
                                            color: Colors.white,
                                          ),
                                        ),
                                        Text(
                                          "30",
                                          style: TextStyle(
                                              fontSize: 16.0,
                                              color: Colors.white,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 8.0),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    Icon(
                                      Icons.account_circle,
                                      color: Colors.white,
                                      size: 23.0,
                                    ),
                                    SizedBox(
                                      width: 12.0,
                                    ),
                                    Column(
                                      children: <Widget>[
                                        Text(
                                          "Money Earned",
                                          style: TextStyle(
                                              fontSize: 16.0,
                                              color: Colors.white),
                                        ),
                                        Text(
                                          "700 Br",
                                          style: TextStyle(
                                              fontSize: 16.0,
                                              color: Colors.white,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 5.0),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    Icon(
                                      Icons.local_parking,
                                      color: Colors.white,
                                      size: 23.0,
                                    ),
                                    SizedBox(
                                      width: 12.0,
                                    ),
                                    Column(
                                      children: <Widget>[
                                        Text(
                                          "Total parked cars",
                                          style: TextStyle(
                                              fontSize: 16.0,
                                              color: Colors.white),
                                        ),
                                        Text(
                                          "70",
                                          style: TextStyle(
                                              fontSize: 16.0,
                                              color: Colors.white,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(15.0),
              child: InkWell(
                onTap: () => {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => Profile()))
                },
                child: Container(
                  width: double.infinity,
                  decoration: BoxDecoration(
                      color: Color.fromARGB(255, 253, 152, 39),
                      borderRadius: BorderRadius.circular(30)),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Stack(
                          children: <Widget>[
                            Icon(
                              Icons.notifications,
                              color: Colors.red,
                              size: 40.0,
                            ),
                            Positioned(
                              right: 0,
                              child: Container(
                                padding: EdgeInsets.all(1),
                                decoration: BoxDecoration(
                                  color: Colors.yellow[300],
                                  borderRadius: BorderRadius.circular(6),
                                ),
                                constraints: BoxConstraints(
                                  minWidth: 20,
                                  minHeight: 16,
                                ),
                                child: Text(
                                  '3',
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 12,
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            )
                          ],
                        ),
                        SizedBox(width: 36.0),
                        Text(
                          "Requests",
                          style: TextStyle(color: Colors.white, fontSize: 20.0),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(height: 5.0),
            Expanded(
              flex: 2,
              child: GridView.count(
                primary: false,
                shrinkWrap: true,
                padding: const EdgeInsets.all(20),
                crossAxisSpacing: 20,
                mainAxisSpacing: 20,
                crossAxisCount: 2,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.all(8),
                    decoration: BoxDecoration(
                        color: Color.fromARGB(255, 115, 47, 181),
                        borderRadius: BorderRadius.circular(12)),
                    child: InkWell(
                      onTap: () => {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ParkingHistory()))
                      },
                      child: Center(
                          child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Icon(
                            Icons.history,
                            color: Colors.white,
                            size: 30.0,
                          ),
                          SizedBox(height: 15.0),
                          Text('Parking History'),
                        ],
                      )),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(8),
                    decoration: BoxDecoration(
                        color: Color.fromARGB(255, 31, 187, 186),
                        borderRadius: BorderRadius.circular(12)),
                    child: InkWell(
                      onTap: () => {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ParkingRequest()))
                      },
                      child: Center(
                          child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Icon(
                            Icons.bookmark_border,
                            color: Colors.white,
                            size: 30.0,
                          ),
                          SizedBox(height: 15.0),
                          Text('Reservations'),
                        ],
                      )),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(8),
                    decoration: BoxDecoration(
                        color: Color.fromARGB(255, 97, 113, 234),
                        borderRadius: BorderRadius.circular(12)),
                    child: InkWell(
                      onTap: () => {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) => MapPage()))
                      },
                      child: Center(
                          child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Icon(
                            Icons.hotel,
                            color: Colors.white,
                            size: 30.0,
                          ),
                          SizedBox(height: 15.0),
                          Text('Google Map'),
                        ],
                      )),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(8),
                    decoration: BoxDecoration(
                        color: Color.fromARGB(255, 254, 204, 48),
                        borderRadius: BorderRadius.circular(12)),
                    child: InkWell(
                      onTap: () => {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) => Rating()))
                      },
                      child: Center(
                          child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Image.asset('assets/review_icon.png'),
                          SizedBox(height: 15.0),
                          Text('Review',style: TextStyle(color: Colors.white,fontSize:15),),
                        ],
                      )),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
        drawer: Drawer(),
      ),
    );
  }
}
