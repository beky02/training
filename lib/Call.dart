import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:url_launcher/url_launcher.dart';

class Call extends StatelessWidget {
  final String phone = 'tel:+251915629212';
  _callPhone() async {
    if (await canLaunch(phone)) {
      await launch(phone);
    } else {
      throw 'Could not call Phone';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Phone'),
      ),
      body: SpinKitSquareCircle(
        color: Colors.red,
        size: 50.0,
      ),
    );
  }
}
