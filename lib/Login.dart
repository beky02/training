import 'package:flutter/material.dart';
import 'package:training/Auth/Authentication.dart';
import 'package:training/ConfirmLogin.dart';

class Login extends StatefulWidget {
  final BaseAuth auth;

  final VoidCallback loginCallback;

  Login({this.auth, this.loginCallback});

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final textController = TextEditingController();
  bool _isShow = true;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: Column(children: <Widget>[
          SizedBox(height: 25),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              width: MediaQuery.of(context).size.width,
              child: Card(
                color: Colors.white,
                shape: RoundedRectangleBorder(
                    borderRadius:
                        BorderRadius.only(bottomLeft: Radius.circular(100))),
                child:  Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Image.asset('assets/title.png'),
                    SizedBox(height:20),
                    Image.asset('assets/logo1.png'),
                  ],
                ),
                elevation: 2,
              ),
              height: 350.0,
              decoration: BoxDecoration(
                borderRadius:
                    BorderRadius.only(bottomLeft: Radius.circular(0.0)),
              ),
            ),
          ),
          Padding(
            padding:
                const EdgeInsets.symmetric(horizontal: 30.0, vertical: 20.0),
            child: Card(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(30)),
              child: TextFormField(
                controller: textController,
                  autofocus: false,
                  decoration: InputDecoration(
                    hintText: "  915629212",
                    fillColor: Colors.white,
                    filled: false,
                    enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.white, width: 0)),
                    focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.white, width: 0.0),
                        borderRadius: BorderRadius.circular(30)),
                    prefixIcon: Padding(
                      padding: const EdgeInsets.only(left: 5.0, ),
                      child: Image.asset('assets/Flag_of_Ethiopia.png')
                    ),
                  ),
                  onSaved: (String value){
                    Auth().singIn(value, "123456").then((onValue){print(onValue);});
                  },),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(right: 40, bottom: 0),
            child: Align(
                alignment: Alignment.centerRight,
                child: Text("login in with email?",
                    style: TextStyle(fontSize: 20, color: Colors.black54))),
          ),
          SizedBox(
            height: 100,
          ),
         _isShow ?
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 45.0),
            child: Container(
              width: double.infinity,
              height: 45,
              child: FlatButton(
                onPressed: () {
                  // setState(() {
                  //   _isShow = false;
                  // });
                  // Auth().signUp(textController.text, "123456").then((onValue){
                  //   print(onValue);
                  //   setState(() {
                  //     _isShow = true;
                  //   });
                     
                  // });
                   Navigator.push(context, MaterialPageRoute(builder: (context) => ConfirmLogin()));
                },
                child: Text(
                  "Login",
                  style: TextStyle(color: Colors.white, fontSize: 22.0),
                ),
                textColor: Colors.white,
                color: Color.fromARGB(255, 98, 100, 249),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(30)
                ),
              ),
            ),
          )
          :
          CircularProgressIndicator(backgroundColor: Colors.amber,)
        ]),
      ),
    );
  }
}
