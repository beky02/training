import 'package:flutter/material.dart';

class ParkingRequest extends StatelessWidget {
  final List<String> entries = <String>[
    'A',
    'B',
    'C',
    'D',
    'E',
    'F',
    'G',
    'H',
    'I',
    'J',
    'K',
    'L',
    'M'
  ];
  final List<int> colorCodes = <int>[600, 500, 50];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        title: Text("Parking Request"),
      ),
      body: Stack(
        children: <Widget>[
          Container(
            color: Color(0xff4F5DF6),
            height: 300.0,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 15.0, right: 15.0, top: 180.0),
            child: Container(
              height: double.infinity,
              width: double.infinity,
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(50),
                      topRight: Radius.circular(50))),
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 20.0, horizontal: 30.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Text(
                          "Reject All",
                          style:
                              TextStyle(color: Color(0xff4F5DF6), fontSize: 20),
                        )
                      ],
                    ),
                  ),
                  Expanded(
                    child: ListView.separated(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      itemCount: entries.length,
                      itemBuilder: (BuildContext context, int index) {
                        return Card(
                          child: Container(
                            height: 110,
                            color: Colors.white,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                SizedBox(width: 20),
                                Row(
                                  children: <Widget>[
                                    CircleAvatar(
                                      backgroundImage: NetworkImage(
                                          "https://image.shutterstock.com/z/stock-vector-person-gray-photo-placeholder-man-in-a-costume-on-white-background-1259815156.jpg"),
                                      radius: 30.0,
                                    ),
                                  ],
                                ),
                                SizedBox(width: 10),
                                Expanded(
                                  child: Column(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceEvenly,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text("Abel Tilahun",
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 20)),
                                      Row(
                                        children: <Widget>[
                                          Text(
                                            'For: ',
                                            style: TextStyle(fontSize: 16),
                                          ),
                                          Text('Nov 4th 2019',
                                              style: TextStyle(
                                                  fontSize: 16,
                                                  fontWeight: FontWeight.w200)),
                                        ],
                                      ),
                                      Row(
                                        children: <Widget>[
                                          Text('Starting: ',
                                              style: TextStyle(fontSize: 16)),
                                          Text('9:00 PM',
                                              style: TextStyle(
                                                  fontSize: 16,
                                                  fontWeight: FontWeight.w200)),
                                        ],
                                      ),
                                      Row(
                                        children: <Widget>[
                                          Text('To: ',
                                              style: TextStyle(fontSize: 16)),
                                          Text('9:00 PM',
                                              style: TextStyle(
                                                  fontSize: 16,
                                                  fontWeight: FontWeight.w200)),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                                Text('2 min',
                                    style: TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.w200)),
                                SizedBox(width: 20)
                              ],
                            ),
                          ),
                        );
                      },
                      separatorBuilder: (BuildContext context, int index) =>
                          const Divider(),
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
