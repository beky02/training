import 'package:flutter/material.dart';

import 'Home.dart';

class ConfirmLogin extends StatefulWidget {
  @override
  _ConfirmLoginState createState() => _ConfirmLoginState();
}

class _ConfirmLoginState extends State<ConfirmLogin> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(children: <Widget>[
          SizedBox(height: 25),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              child: Card(
                color: Colors.white,
                shape: RoundedRectangleBorder(
                    borderRadius:
                        BorderRadius.only(bottomLeft: Radius.circular(100))),
                child: Column(
                  children: <Widget>[
                    Expanded(child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                         Image.asset('assets/title.png'),
                    SizedBox(height:20),
                        Image.asset('assets/logo1.png'),
                      ],
                    )),
                    Padding(
                      padding: const EdgeInsets.only(bottom:20.0,right: 30),
                      child: Align(
                        alignment: Alignment.bottomRight,
                        child: Text('Confirm phone',style: TextStyle(fontSize:21,color: Colors.black54))
                      ),
                    )
                  ],
                ),
                elevation: 2,
              ),
              height: 350.0,
              decoration: BoxDecoration(
                borderRadius:
                    BorderRadius.only(bottomLeft: Radius.circular(0.0)),
              ),
            ),
          ),
          SizedBox(height: 60,),
          Padding(
            padding:
                const EdgeInsets.symmetric(horizontal: 90.0, vertical: 20.0),
            child: Card(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(0)),
              child: TextFormField(
                  autofocus: false,
                  decoration: InputDecoration(
                    hintStyle: TextStyle(color:Colors.amber,letterSpacing:5,),
                    hintText: "__  __  __  __",
                    fillColor: Colors.white,
                    filled: false,
                    enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.white,),
                        borderRadius: BorderRadius.circular(10)),
                        
                    focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.white,),
                        borderRadius: BorderRadius.circular(3)),
                    
                  )),
            ),
          ),
         
          SizedBox(
            height: 70,
          ),
         
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 45.0),
            child: Container(
              width: double.infinity,
              height: 45,
              child: FlatButton(
                onPressed: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) => Home()));
                },
                child: Text(
                  "Confirm",
                  style: TextStyle(color: Colors.white, fontSize: 22.0),
                ),
                textColor: Colors.white,
                color: Color.fromARGB(255, 98, 100, 249),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(30)
                ),
              ),
            ),
          )
        ]),
      );
  }
}