import 'package:flutter/material.dart';

class Rating extends StatefulWidget {
  @override
  _RatingState createState() => _RatingState();
}

class _RatingState extends State<Rating> {
  final List<String> entries = <String>[
    'A',
    'B',
    'C',
    'A',
    'B',
    'C',
    'A',
    'B',
    'C'
  ];
  int selected;

  void _getIndex(index) {
    print(index);
    setState(() {
      selected = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        title: Text("Rating"),
      ),
      body: Stack(
        children: <Widget>[
          Container(
            color: Color(0xffFECC30),
            height: 300.0,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 15.0, right: 15.0, top: 180.0),
            child: Container(
              height: double.infinity,
              width: double.infinity,
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(50),
                      topRight: Radius.circular(50))),
              child: Column(
                children: <Widget>[
                  SizedBox(height: 40),
                  Expanded(
                    child: ListView.separated(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      itemCount: entries.length,
                      itemBuilder: (BuildContext context, int index) {
                        return Column(
                          mainAxisSize: MainAxisSize.max,
                          children: <Widget>[
                            Container(
                              color: Colors.white,
                              child: Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: <Widget>[
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: <Widget>[
                                      SizedBox(width: 20),
                                      Row(
                                        children: <Widget>[
                                          CircleAvatar(
                                            backgroundImage: NetworkImage(
                                                "https://image.shutterstock.com/z/stock-vector-person-gray-photo-placeholder-man-in-a-costume-on-white-background-1259815156.jpg"),
                                            radius: 30.0,
                                          ),
                                        ],
                                      ),
                                      SizedBox(width: 20),
                                      Expanded(
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceEvenly,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Row(
                                              children: <Widget>[
                                                Text("Kamila ",
                                                    style: TextStyle(
                                                        fontSize: 20)),
                                                SizedBox(width: 10),
                                                Text('Nov 4th 2019',
                                                    style: TextStyle(
                                                        fontSize: 16,
                                                        fontWeight:
                                                            FontWeight.w200)),
                                              ],
                                            ),
                                            Row(
                                              children: <Widget>[
                                                Icon(
                                                  Icons.star,
                                                  color: Colors.yellow,
                                                  size: 18,
                                                ),
                                                Icon(
                                                  Icons.star,
                                                  color: Colors.yellow,
                                                  size: 18,
                                                ),
                                                Icon(
                                                  Icons.star,
                                                  color: Colors.yellow,
                                                  size: 18,
                                                ),
                                                Icon(
                                                  Icons.star,
                                                  color: Colors.black12,
                                                  size: 18,
                                                ),
                                                Icon(
                                                  Icons.star,
                                                  color: Colors.black12,
                                                  size: 18,
                                                ),
                                              ],
                                            )
                                          ],
                                        ),
                                      ),
                                      if(selected != index)
                                      Column(
                                        children: <Widget>[
                                          Icon(Icons.flag,
                                              color: Colors.redAccent),
                                          SizedBox(height: 16),
                                          InkWell(
                                            onTap: () {
                                              _getIndex(index);
                                            },
                                            child: Text('Replay',
                                                style: TextStyle(
                                                  color: Color(0xff2E0BEE),
                                                  fontSize: 18,
                                                )),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                  Text(
                                      'The parking space is crouded and a little narrow'),
                                  SizedBox(height: 15),
                                  if (selected == index)
                                    TextField(
                                        decoration: InputDecoration(
                                      hintText: "Enter Message ",
                                      border: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(0),
                                          ),
                                      fillColor: Colors.white,
                                      filled: true,
                                      enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Colors.yellow, width: 5.0),
                                      ),
                                      focusedBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: Colors.yellow,width: 5.0)
                                      ),
                                      suffixIcon: Icon(
                                        Icons.send,
                                        color: Colors.yellow,
                                      ),
                                    )),
                                ],
                              ),
                            ),
                          ],
                        );
                      },
                      separatorBuilder: (BuildContext context, int index) =>
                          const Divider(),
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
